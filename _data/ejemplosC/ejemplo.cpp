#include <iostream>
#include <stdio.h>
#include "omp.h"
#include <string>
#include <sstream>
#include <iomanip>

#include <fstream>

int main(){

	/*
	unsigned int n;
	int i = 0;
	for( n = 3; n >= 0 & i<10; --n & ++i){
		std::cout << n << "\n";
	}
	*/
/*


	int mm [] = {1,2,3,4};

	int vectors [5]	;

	int* primer = mm;
	int* primer2 = primer;

	int* segundo = &mm[1];
	int* segundo2 = segundo;

	int temp = *primer;

	std::cout << "primeros:\t" << *primer << "\t" << primer << " | " << *primer2 << "\t" << primer2 << "\n"; 
	std::cout << "segundos:\t" << *segundo << "\t" << segundo << " | " << *segundo2 << "\t" << segundo2 << "\n"; 
	std::cout << "temporal:\t" << temp << "\t" << &temp  << "\n"; 

	temp = *primer;
	*primer = *segundo;
	*segundo = temp;

	std::cout << "temporal:\t" << temp << "\t" << &temp  << "\n"; 
	std::cout << "primeros:\t" << *primer << "\t" << primer << " | " << *primer2 << "\t" << primer2 << "\n"; 
	std::cout << "segundos:\t" << *segundo << "\t" << segundo << " | " << *segundo2 << "\t" << segundo2 << "\n"; 


	int x = 1;
	std::string st = "";
	st = std::to_string(x );
	st = st  + + " h \n";
	std::cout << st << std::endl;
*/

	/*

	/// OPENMP EXAMPLE
	int num_threads , threadid;

	#pragma omp parallel
	{
		num_threads = omp_get_num_threads();

		threadid = omp_get_thread_num();

		printf("\nHelloWorld from %d",threadid);
	}
	printf("\nProgram Exit\n");


	*/


	/*
	/// FORMATTING NUMBERS SO THAT ALL HAVE 3 DIGITS
	std::stringstream ss;
	
	for (int k = 9; k<100000;k=k*10){
		ss << std::setw(5) << std::setfill('0') << k;
		std::cout << ss.str() << std::endl;
		ss.str("");
		ss.clear();
		
	}
*/



	float m = 0.1f;
	float k = 20.0f;

	float j = 15.0f;

	bool convergence = false;

	for(int i = 0; (i < 100)&&(!convergence); ++i){
		j+=m;
		if (k-j < 0.2f){
			convergence = true;
		}
	}


	std::cout << "convergence = " << convergence << std::endl;

	std::ofstream myfile;
	myfile.open ("example.txt");
	myfile << "Writing that.\n";
	myfile << "Writing this too.\n";
	
	myfile.close();








	return 0;
}